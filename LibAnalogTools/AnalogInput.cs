﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibAnalogTools
{
    class AnalogInput
    {
        //Attributs privée
        private int channel;
        private ushort mesureBrute;
        private float mesureVolt;
        private MccBoard acqBoard;


        //Constructeurs
        public AnalogInput() { }
        public AnalogInput(MccBoard board, int channel = 0)
        {
            this.acqBoard = board;
            this.channel = channel;
        }

        //Accesseurs
        //Utilisation du set : maVariable.Channel = Console.Read();
        //Utilisation du get : Console.Write(maVariable.Channel);
        public int Channel { get => channel; set => channel = value; }
        public ushort MesureBrute { get => mesureBrute; set => mesureBrute = value; }
        public float MesureVolt { get => mesureVolt; set => mesureVolt = value; }
        public MccBoard AcqBoard { get => acqBoard; set => acqBoard = value; }
    }
}
