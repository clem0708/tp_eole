﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MccDaq;

namespace LibAnalogTool
{
    public class AnalogInput
    {
        //Attributs privée
        private int _channel;
        private ushort _mesureBrute;
        private float _mesureVolt;
        private MccBoard _acqBoard;


        //Constructeurs
        public AnalogInput() { }
        public AnalogInput(MccBoard board, int channel = 0)
        {
            this._acqBoard = board;
            this._channel = channel;
        }

        //Accesseurs
        public int GetChannel()
        {
            return this._channel;
        }
        public ushort MesureBrute()
        {
            _acqBoard.AIn(7, Range.Bip10Volts, out _mesureBrute);
            return _mesureBrute;
        }
        public float MesureVolt()
        {
            _acqBoard.ToEngUnits(Range.Bip10Volts, _mesureBrute, out _mesureVolt);
            return this._mesureVolt;
        }
    }
}
