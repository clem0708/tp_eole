﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Entrainement_Thread
{
    class DecouverteThreadConsole
    {
        public static bool stop = false;

        static void Main(string[] args)
        {
            //Durée en secondes du thread principal
            int dureeMain = 5;

            //Le thread lui-même, création sans le démarrer
            ThreadStart delegateRunLetter = new ThreadStart(RunLetter);
            Thread laTache = new Thread(delegateRunLetter);

            //Demarrage des threads
            Console.WriteLine("> Le thread est créé. \n> Démarrage...");
            laTache.Start();

            //Le thread principal compte le temps
            Console.WriteLine("> Main en route pour {0} secondes...", dureeMain);
            for (int i = 1; i < dureeMain; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> Main : " + i + " sec");
            }

            stop = true;
            laTache.Join();
            Console.WriteLine("> Le thread LaTache est terminé.");
            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();
        }

        public static void RunLetter()
        {
            ///Un random pour le sleep
            Random random = new Random();

            ///La lettre à afficher
            char letter = 'a';

            Console.WriteLine("\t\t---> Le RunLetter commence son travail.");
            while (!stop)
            {
                if (letter < 'z')
                    letter++;
                else
                    letter = 'a';

                Console.WriteLine("\t\t---> Letter : " + letter.ToString());

                // Dodo pour un temps aléatoire < 1267 ms
                Thread.Sleep(random.Next(1267));
            }
            Console.WriteLine("\t\t---> Le RunLetter a fini son travail.");
        }
    }
}
