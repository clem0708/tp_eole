﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadLetter
    {
        //Déclaration des variables
        //public bool stop = false;
        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);
        public AutoResetEvent SignalVoyelle { get; } = new AutoResetEvent(false);

        //Initialisation du thread
        public Thread laTache;

        public MyThreadLetter()
        {
            //Création d'un thread sans le démarrer
            laTache = new Thread(new ThreadStart(RunLetter));
        }

        public void Start()
        {
            try
            {
                laTache.Start();
                Console.WriteLine("Letter démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Letter à déjà était démarré !!");
            }
        }

        //public void Stop()
        //{
        //    stop = true;
        //    Console.WriteLine("> Arrêt du thread");
        //}

        public void RunLetter()
        {
            Random randomTime = new Random();
            char letter = 'a';

            Console.WriteLine("\t\t---> Le RunLetter commence son travail.");
            while (!SignalExit.WaitOne(randomTime.Next(100, 1001), false))
            {
                Random randomLetter = new Random();
                char randomChar = (char)randomLetter.Next('a', 'z');

                Console.WriteLine("\t\t---> Letter : " + randomChar.ToString());

                Thread.Sleep(randomTime.Next(1276));
            }

            letter = (char)randomTime.Next(97, 123);
            switch (letter)
            {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    SignalVoyelle.Set();
                    break;

                default:
                    break;
            }
        }

    }
}
