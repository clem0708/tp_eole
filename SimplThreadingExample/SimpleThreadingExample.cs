﻿using SimplThreadingExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class SimpleThreadingExample
    {
        static void Main(string[] args)
        {
            int iter = 15;

            //Création des objets encapsulant les threads
            MyThreadLetter ThLetter = new MyThreadLetter();
            Console.WriteLine("> Thread letter crée");
            MyThreadCompteurVoyelles ThCompteur = new MyThreadCompteurVoyelles(ThLetter);
            Console.WriteLine("> Thread compteur crée");

            ThLetter.Start();
            ThCompteur.Start();

            Console.WriteLine("> En route pour {0} sec ...", iter);
            for(int i = 0; i<iter; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> Main : " + i + " sec");
            }

            Console.WriteLine("> Demande d'arrêt du thread letter");
            ThLetter.SignalExit.Set();
            Console.WriteLine("> Demande d'arrêt du thread compteur");
            ThCompteur.SignalExit.Set();

            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();
        }
    }
}
