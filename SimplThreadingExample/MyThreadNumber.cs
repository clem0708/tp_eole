﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadNumber
    {
        public bool stop = false;
        public Random random = new Random();
        public int number = 0;
        public Thread laTache;
        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);

        public MyThreadNumber()
        {
            laTache = new Thread(new ParameterizedThreadStart(RunNumber));
        }

        public void RunNumber(object max)
        {
            while (!SignalExit.WaitOne(random.Next(100, 1001), false))
            {
                if (number < (int)max)
                    number++;
                else
                    number = 0;

                Console.WriteLine("\t\t\tNumber : " + number.ToString());

                Thread.Sleep(random.Next(323));
            }
            Console.WriteLine("Le thread number a fini son travail");
        }

        public void Start( int max = 999)
        {
            try
            {
                laTache.Start(max);
                Console.WriteLine("Number démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Number déjà démarré !!");
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Number ne peut pas démarrer par manque de mémoire");
                throw ex;
            }
        }

        public void Stop()
        {
            stop = true;
            Console.WriteLine("> Arrêt du thread");
        }
    }
}
