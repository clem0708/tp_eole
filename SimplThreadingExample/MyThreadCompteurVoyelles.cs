﻿using SimpleThreadingExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimplThreadingExample
{
    class MyThreadCompteurVoyelles
    {
        public MyThreadLetter threadLetter = new MyThreadLetter();
        public AutoResetEvent SignalExit = new AutoResetEvent(false);
        private int compteurVoyelle = 0;
        public object verrou;
        public Thread laTache;

        public MyThreadCompteurVoyelles(MyThreadLetter threadLetter)
        {
            threadLetter = this.threadLetter;
            laTache = new Thread(new ThreadStart(RunCompteurVoyelle));
        }

        private void RunCompteurVoyelle()
        {
            WaitHandle[] lesSignaux = { threadLetter.SignalExit, threadLetter.SignalVoyelle };
            int indexSignal = -1;
            bool fin = false;

            Console.WriteLine("\t\tLe RunCompteurVoyelle à démarré...");
            while (!fin)
            {
                indexSignal = WaitHandle.WaitAny(lesSignaux);
                switch (indexSignal)
                {
                    //SignalVoyelle activé
                    case 1:
                        lock (verrou)
                        {
                            compteurVoyelle++;
                            Console.WriteLine("\t\t ---> Compteur : " + compteurVoyelle.ToString());
                        }
                        break;
                    //SignalExit activé
                    case 0:
                        fin = true;
                        break;
                }
            }
        }

        public void Start()
        {
            try
            {
                laTache.Start();
                Console.WriteLine("Letter démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Letter à déjà était démarré !!");
            }
        }
    }
}
