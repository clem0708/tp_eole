﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MccDaq;
using LibAnalogTool;
using System.Threading;

namespace TestAnalogInput
{
    class TestAnalogInput
    {
        private MccBoard acqBoard;
        private AnalogInput lightIn;

        public TestAnalogInput()
        {
            this.acqBoard = new MccBoard(0);
            this.lightIn = new AnalogInput(this.acqBoard, 7);
        }


        static void Main(string[] args)
        {
            TestAnalogInput testAnalogInput = new TestAnalogInput();
            testAnalogInput.Run();
        }        
        
        public void Run()
        {
            ushort mesB = 0;
            float mesV = 0;
            bool boucle = true;

            do
            {
                //Acquisition et affichage de mesure brute
                mesB = lightIn.MesureBrute();
                Console.WriteLine(mesB.ToString());

                mesV = lightIn.MesureVolt();
                Console.WriteLine(mesV.ToString("F2") + " Volts");

                Thread.Sleep(1000);

            } while (boucle != false);
        }
    }
}
