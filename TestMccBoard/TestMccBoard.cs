﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibCapteurAn;
using MccDaq;

namespace TestMccBoard
{
    class TestMccBoard
    {
        static void Main(string[] args)
        {
            MccDaq.MccBoard mccBoard = new MccDaq.MccBoard(0);

            float EngUnits;
            MccDaq.ErrorInfo ULStat;
            UInt16 DataValue;
            int boucle = 1;

            //Boucle infinie
            do
            {
                //Acquisition des données
                ULStat = mccBoard.AIn(7, Range.Bip10Volts, out DataValue);

                //Conversion des données
                ULStat = mccBoard.ToEngUnits(Range.Bip10Volts, DataValue, out EngUnits);

                Console.WriteLine(DataValue.ToString()); //Affichage de la valeur récupérée
                Console.WriteLine(EngUnits.ToString("F2") + " Volts"); //Affichage de la valeur convertie

                Thread.Sleep(1000); //Affichage des valeurs toutes les 1s
            } while (boucle != 6);

        }
    }
}